# Resumo

A aplicação é uma Single Page Application escrita em TypeScript e usando React,
Vite e CSS.

# Instruções para rodar

É preciso rodar o servidor do backend e do front:

```sh
npm run start &
npm run dev
```

# Tecnologias usadas

## Linguagem

- TypeScript.

## Framework

- React, para construir a interface.

## Ferramentas

- Vite, para fazer o bundle da aplicação em desenvolvimento e produção.

## Bibliotecas
- `react-router`, para fazer roteamento no cliente.
- `react-query`, para fazer a requisição HTTP.
- `remeda`, para manipulação de arrays, objetos etc.
- `lodash`, para algumas utilidades.

# Detalhes de implementação

Para fazer as requisições eu utilizei a biblioteca `react-query`. A resposta é
então usada para mostrar os produtos na tela. Os produtos podem ser filtrados
pelo usuário, o que é usando a Context API do React para manipular um objeto em
um Provider -- que contém os filtros e uma função para manipular os filtros.

Utilizei CSS puro para fazer os estilos. [O Vite simplesmente pega esse CSS e
injeta na página em uma `<style>`
tag](https://vitejs.dev/guide/features.html#css), então nada garante que o
estilo usado em um componente não vá afetar outros (como com CSS Modules). Não
acho o ideal, normalmente usaria alguma biblioteca de CSS-in-JS ou zero-runtime
CSS, mas foi o mais fácil para eu iterar fácil sem me preocupar com a tooling
necessária.

# O que poderia ter ido melhor?

- Usar CSS custom properties (CSS variables) desde o início.
- SEO não-existente praticamente.
