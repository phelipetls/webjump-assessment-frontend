import { Link } from 'react-router-dom'

import './Header.css'
import WebjumpLogo from '../../assets/images/webjump-compass-min-2.png'

export function Header() {
  return (
    <header>
      <div className='login-or-signup'>
        <Link to='/login'>Acesse sua conta</Link> ou{' '}
        <Link to='/signin'>Cadastre-se</Link>
      </div>

      <div className='row'>
        <span className='hamburguer-icon'></span>

        <Link to='/'>
          <img className='webjump-logo' src={WebjumpLogo} />
        </Link>

        <span className='search-icon'></span>

        <form className='search-form'>
          <input name='search-input' />

          <button form='search-form' type='submit'>
            Buscar
          </button>
        </form>
      </div>
    </header>
  )
}
