import { Link } from 'react-router-dom'

import './NavBar.css'
import { useProductCategories } from '../../hooks/useProductCategories'

export function NavBar() {
  const { data } = useProductCategories()

  return (
    <nav className='main-nav'>
      <ul>
        <li>
          <Link to='/'>Página Inicial</Link>
        </li>

        {data &&
          data.items.map((category) => (
            <li key={category.id}>
              <Link to={`/category/${category.id}`}>{category.name}</Link>
            </li>
          ))}

        <li>
          <Link to='/contato'>Contato</Link>
        </li>
      </ul>
    </nav>
  )
}
