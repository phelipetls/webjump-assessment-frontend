import { Routes, Route } from 'react-router-dom'

import { Header, NavBar, Footer } from './components'
import { HomePage, ProductCategory } from './views'
import './App.css'

function App() {
  return (
    <div className='app-container'>
      <Header />
      <NavBar />
      <div className='app-content'>
        <Routes>
          <Route path='/' element={<HomePage />} />
          <Route path='/category/:categoryId' element={<ProductCategory />} />
        </Routes>
      </div>
      <Footer />
    </div>
  )
}

export default App
