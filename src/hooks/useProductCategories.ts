import { useQuery } from 'react-query'

import { ProductCategoriesPayload } from '../types'

export function useProductCategories() {
  return useQuery<ProductCategoriesPayload>(
    ['productCategoryItems'],
    async () => {
      const response = await fetch(
        `http://localhost:8888/api/V1/categories/list`
      )
      if (!response.ok) {
        throw new Error()
      }
      const json = await response.json()
      return json
    }
  )
}
