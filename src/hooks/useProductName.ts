import { useProductCategories } from './useProductCategories'

export const useProductName = (categoryId: string): string => {
  const productCategories = useProductCategories()

  if (!productCategories.data) {
    return 'Carregando...'
  }

  const productName =
    productCategories.data.items.find(
      (category) => category.id === Number(categoryId)
    )?.name || 'Produto desconhecido'

  return productName
}
