import { useParams } from 'react-router-dom'
import { useQuery } from 'react-query'

import './ProductCategory.css'
import {
  Breadcrumbs,
  Filters,
  Divider,
  Controls,
  FilterContextProvider,
  ProductGrid,
} from './components'
import { ProductPayload } from '../../types'
import { useProductName } from '../../hooks'

export function ProductCategory() {
  const { categoryId } = useParams()

  const { data, isLoading, isError } = useQuery<ProductPayload>(
    ['productCategory', categoryId],
    async () => {
      const response = await fetch(
        `http://localhost:8888/api/V1/categories/${categoryId}`
      )
      if (!response.ok) {
        throw new Error()
      }
      const json = await response.json()
      return json
    }
  )

  const productName = useProductName(categoryId!)

  if (isLoading) {
    return <h2>Carregando...</h2>
  }

  if (isError) {
    return <h2>Ops, ocorreu um erro :(</h2>
  }

  return (
    <div className='product-category-container'>
      <Breadcrumbs />

      <div className='product-category-content-container'>
        <FilterContextProvider>
          <Filters filters={data!.filters} items={data!.items} />

          <main className='product-category-content'>
            <h1>{productName}</h1>

            <Divider />

            <Controls />

            <ProductGrid products={data!.items} />
          </main>
        </FilterContextProvider>
      </div>
    </div>
  )
}
