import { NavLink, useParams } from 'react-router-dom'

import './Breadcrumbs.css'
import { useProductName } from '../../../../hooks'

export function Breadcrumbs() {
  const { categoryId } = useParams()

  const productName = useProductName(categoryId!)

  return (
    <nav className='breadcrumbs'>
      <ol>
        <NavLink to='/'>Página Inicial</NavLink>

        <NavLink to={`/category/${categoryId}`}>{productName}</NavLink>
      </ol>
    </nav>
  )
}
