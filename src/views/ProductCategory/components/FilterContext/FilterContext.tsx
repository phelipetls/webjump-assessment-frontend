import React, { useContext } from 'react'
import { FilterObject } from '../../../../types'

type SetFilterAction = {
  type: 'SET_FILTER'
  payload: {
    categoryId: string
    filter: Record<string, string>
  }
}

function isSetFilterAction(action: Action): action is SetFilterAction {
  return action.type === 'SET_FILTER'
}

type SetOrderAction = {
  type: 'SET_ORDER'
  payload: {
    orderBy: 'price',
    order: 'asc' | 'desc'
  }
}

function isSetOrderAction(action: Action): action is SetOrderAction {
  return action.type === 'SET_ORDER'
}

type Action = SetFilterAction | SetOrderAction

type State = {
  filters: Record<string, FilterObject>,
  orderBy: 'price',
  order: 'asc' | 'desc',
}

const initialState = {
  filters: {},
  orderBy: 'price' as const,
  order: 'asc' as const
}

type FilterContextValue = {
  state: State
  dispatch: React.Dispatch<Action>
}

const FilterContext = React.createContext<FilterContextValue>({
  state: initialState,
  dispatch: () => {},
})

function reducer(state: State, action: Action) {
  if (isSetFilterAction(action)) {
    const { categoryId, filter } = action.payload
    return {
      ...state,
      filters: {
        [categoryId]: {
          ...state.filters[categoryId],
          ...filter,
        },
      }
    }
  }
  if (isSetOrderAction(action)) {
    const { orderBy, order } = action.payload
    return {
      ...state,
      orderBy,
      order,
    }
  }
  return state
}

export const FilterContextProvider: React.FC = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState)

  return (
    <FilterContext.Provider value={{ state, dispatch }}>
      {children}
    </FilterContext.Provider>
  )
}

export function useFilterContext() {
  return useContext(FilterContext)
}
