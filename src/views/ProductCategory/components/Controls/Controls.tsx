import './Controls.css'
import { useFilterContext } from '../FilterContext'

export function Controls() {
  const { dispatch } = useFilterContext()

  return (
    <div className='product-category-controls'>
      <div className='product-category-controls-view'>
        {/* TODO: get these items from somewhere */}
        <div className='product-category-controls-view-item active'></div>
        <div className='product-category-controls-view-item inactive'></div>
      </div>

      <div className='product-category-controls-order'>
        <label htmlFor='select-order'>Ordenar Por</label>

        <select
          id='select-order'
          onChange={(event) =>
            dispatch({
              type: 'SET_ORDER',
              payload: {
                orderBy: 'price',
                order: event.target.selectedOptions[0]?.text.includes('decrescente') ? 'desc' : 'asc',
              },
            })
          }
        >
          <option>Preço (crescente)</option>
          <option>Preço (decrescente)</option>
        </select>
      </div>
    </div>
  )
}
