import { formatCurrency } from '@brazilian-utils/brazilian-utils';

import './Product.css'
import { ProductItem } from '../../../../../../types';

type Props = {
  product: ProductItem
}

export function Product(props: Props) {
  const { product } = props

  return (
    <div className='product'>
      <img src={`http://localhost:8888/${product.image}`} />

      <h2>{product.name}</h2>

      <div className='product-price-container'>
        {
          product.specialPrice && (
            <div className='product-discount'>
              R$ {formatCurrency(product.specialPrice)}
            </div>
          )
        }

        <div className='product-price'>R$ {formatCurrency(product.price)}</div>
      </div>

      <button>Comprar</button>
    </div>
  )
}
