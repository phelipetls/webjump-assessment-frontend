import { isEmpty } from 'lodash'
import { useParams } from 'react-router-dom'
import * as R from 'remeda'

import './ProductGrid.css'
import { ProductItem } from '../../../../types'
import { useFilterContext } from '../FilterContext'
import { Product } from './components'

type Props = {
  products: ProductItem[]
}

export function ProductGrid(props: Props) {
  const { products } = props

  const { categoryId } = useParams()

  const { state } = useFilterContext()

  let filteredProducts = products

  if (state.filters[categoryId!] && !isEmpty(state.filters[categoryId!])) {
    filteredProducts = products.filter((product) => {
      return product.filter.some((f) => {
        const [key] = Object.keys(f)
        const [value] = Object.values(f)
        return state.filters[categoryId!]?.[key] === value
      })
    })
  }

  if (state.orderBy) {
    filteredProducts = R.sortBy(filteredProducts, [
      state.orderBy === 'price' ? R.prop('price') : R.prop('name'),
      state.order,
    ])
  }

  return (
    <div className='product-grid'>
      {filteredProducts.map((product) => (
        <Product key={product.id} product={product} />
      ))}
    </div>
  )
}
