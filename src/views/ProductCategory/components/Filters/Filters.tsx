import React from 'react'
import * as R from 'remeda'
import { Link, useParams } from 'react-router-dom'

import './Filters.css'
import { FilterObject, ProductItem } from '../../../../types'
import { useProductCategories } from '../../../../hooks/useProductCategories'
import { useFilterContext } from '../FilterContext'

type Props = {
  filters: FilterObject[]
  items: ProductItem[]
}

const colorNameToHex: Record<string, string> = {
  Rosa: '#FFE0E0',
  Preta: '#000000',
  Preto: '#000000',
  Laranja: '#FFA500',
  Amarela: '#FFFF00',
  Azul: '#0000FF',
  Cinza: '#808080',
}

const filterNameTranslation: Record<string, string> = {
  gender: 'Gênero',
}

export function Filters(props: Props) {
  const { filters, items } = props

  const { data } = useProductCategories()

  const { categoryId } = useParams()

  const { dispatch } = useFilterContext()

  return (
    <aside className='filters'>
      <h1 className='filters-title'>Filtre Por</h1>

      <h2 className='filters-subtitle'>Categorias</h2>

      <ul className='filters-list'>
        {data &&
          data.items.map((category) => (
            <li key={category.id}>
              <Link to={`/category/${category.id}`}>{category.name}</Link>
            </li>
          ))}
      </ul>

      {Object.keys(R.mergeAll(filters)).map((filterName) => {
        const itemsFilters = R.flatMap(items, R.prop('filter'))
        const itemsFiltersValues = R.uniq(
          R.map(itemsFilters, R.prop(filterName))
        )

        const setFilterValue = (value: string) =>
          dispatch({
            type: 'SET_FILTER',
            payload: {
              categoryId: categoryId!,
              filter: { [filterName]: value },
            },
          })

        return (
          <React.Fragment key={filterName}>
            <h2 className='filters-subtitle'>
              {filterNameTranslation[filterName] || filterName}
            </h2>

            <ul
              className={
                filterName === 'color' ? 'filters-colors-list' : 'filters-list'
              }
            >
              {itemsFiltersValues.map((value) =>
                filterName === 'color' ? (
                  <li key={value}>
                    <button
                      className='filters-list-color-item'
                      style={{
                        backgroundColor: colorNameToHex[value] || 'red',
                      }}
                      title={`Filtrar pela cor ${value}`}
                      onClick={() => setFilterValue(value)}
                    />
                  </li>
                ) : (
                  <li key={value}>
                    <div
                      className='filters-list-text-item'
                      onClick={() => setFilterValue(value)}
                    >
                      {value}
                    </div>
                  </li>
                )
              )}
            </ul>
          </React.Fragment>
        )
      })}
    </aside>
  )
}
