import { Link } from 'react-router-dom'

import './SideBar.css'
import { useProductCategories } from '../../../../hooks/useProductCategories'

export function SideBar() {
  const { data } = useProductCategories()

  return (
    <aside className='homepage-sidebar'>
      <ul>
        <li>
          <Link to='/'>Página Inicial</Link>
        </li>
        {data &&
          data.items.map((category) => (
            <li key={category.id}>
              <Link to={`/category/${category.id}`}>{category.name}</Link>
            </li>
          ))}
        <li>
          <Link to='/contato'>Contato</Link>
        </li>
      </ul>
    </aside>
  )
}
