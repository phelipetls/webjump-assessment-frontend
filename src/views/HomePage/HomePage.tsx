import { SideBar, Main } from './components'

export function HomePage() {
  return (
    <>
      <SideBar />
      <Main />
    </>
  )
}
