export type FilterObject = Record<string, string>

export type ProductItem = {
  id: number
  sku: string
  path: string
  name: string
  image: string
  price: number
  specialPrice?: number
  filter: FilterObject[]
}

export type ProductPayload = {
  filters: FilterObject[]
  items: ProductItem[]
}

export type ProductCategoryItem = {
  id: number
  name: string
  path: string
}

export type ProductCategoriesPayload = {
  items: ProductCategoryItem[]
}
